import os
import requests
from bs4 import BeautifulSoup
from datetime import datetime

category = input('What category should I look into today? Answer: ')

pages = int(input('\nHow many pages should I look into? Answer: '))

price = input('\nFinally, what price should I help you look out for? Answer: ')

foldername = 'results scraped on {}'.format(datetime.today().date())
try:
    working_folder = os.mkdir(os.path.join(os.getcwd(), foldername))
except FileExistsError:
    pass

results_path = os.path.join(os.getcwd(), foldername) + \
    '/results at {0}.txt'.format(datetime.today().time())
html_path = os.path.join(os.getcwd(), foldername) + \
    '/selected_results at {0}.html'.format(datetime.today().time())

results_txtfile = open(results_path, 'x')
results_htmlfile = open(html_path, 'x')

results_htmlfile.write('''\
    <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <h1>Hello, world!</h1>
''')

categories = dict()
categories['fashion'] = 'https://www.jumia.com.ng/category-fashion-by-jumia/'
categories['phonesandtablets']  = 'https://www.jumia.com.ng/phones-tablets/'
categories['tvandelectronics'] = 'https://www.jumia.com.ng/cameras-electronics/'
categories['computing'] = 'https://www.jumia.com.ng/computing/'
categories['groceries'] = 'https://www.jumia.com.ng/groceries/'
categories['healthandbeauty'] = 'https://www.jumia.com.ng/beauty-corner/'
categories['homeandoffice'] = 'https://www.jumia.com.ng/home-and-living/'
categories['babykidsandtoys'] = 'https://www.jumia.com.ng/baby-kids-toys/'
categories['gamesandconsoles'] = 'https://www.jumia.com.ng/games-consoles/'
categories['automobile'] = 'https://www.jumia.com.ng/automobile/'
categories['watchesandsunglasses'] = 'https://www.jumia.com.ng/watches-sunglasses/'


for page in range(1, pages+1):
    results_txtfile.write(
        '\n\n\n************************************************\n\n\n')
    results_txtfile.write(
        '********************Items in page {0}*****************'.format(page))
    results_txtfile.write(
        '\n\n\n************************************************\n\n\n')

    page_link = categories[category]+'?page={0}'.format(page)

    print(page_link)

    page_response = requests.get(page_link, timeout=5)

    page_content = BeautifulSoup(page_response.content, 'html.parser')

    div = page_content.body.main.find_all('section')

    def find_sku_gallery(css_class):
        return css_class is not None and 'sku -gallery' in css_class

    def find_price_container(css_class):
        return css_class is not None and css_class == 'price'

    # div_1 = page_content.find_all('section')[5].div
    div_1 = page_content.find_all('section')[5].find_all(
        class_="sku -gallery -validate-size ")
    items = page_content.find_all(
        'section')[5].find_all(class_=find_sku_gallery)

    def print_text(item):
        try:
            results_txtfile.write('\nName: {0}\t\t New price: {1}\t\t Old price: {2} \n'.format(
                item[0].text, item[1].text, item[2].text))
            if item[1].text == price:
                return True
        except IndexError:
            if item != []:
                results_htmlfile.write(
                    '<div class="alert alert-warning" role="alert">'+str(item)+'</div>')

    for item in items:
        prices_of_items = item.find_all(dir='ltr')
        if print_text(prices_of_items):
            results_htmlfile.write(
                '<div class="alert alert-success" role="alert">'+str(item)+'</div>')

results_htmlfile.write('''/
    </body>
    </html>
''')

results_txtfile.close()
results_htmlfile.close()

print('\n\nI am done. Thank you for using me :)')
